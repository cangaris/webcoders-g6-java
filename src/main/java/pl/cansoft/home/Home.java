package pl.cansoft.home;

public class Home {
    String address;
    String city;
    Window window;
    Dor dor;

    public Home(String address, String city, Window window, Dor dor) {}

    public static class Window {
        String type;
        public Window(String type) {
            this.type = type;
        }
    }
    public static class Dor {
        String type;
        public Dor(String type) {
            this.type = type;
        }
    }
}
