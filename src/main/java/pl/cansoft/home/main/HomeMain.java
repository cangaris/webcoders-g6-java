package pl.cansoft.home.main;

import pl.cansoft.home.Home;

public class HomeMain {
    public static void main(String[] args) {

        var home = new Home("Warszawska 3/7", "Kraków",
            new Home.Window("Type 1"),
            new Home.Dor("Type 3")
        );
    }
}
