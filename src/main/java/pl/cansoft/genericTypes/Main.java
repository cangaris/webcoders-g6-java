package pl.cansoft.genericTypes;

abstract class Fruit {}
interface Eatable {
    void eat();
}

class Strawberry extends Fruit implements Eatable {
    String type;
    Strawberry(String type) {
        this.type = type;
    }

    @Override
    public void eat() {
        System.out.println("I eat strawberry...");
    }

    @Override
    public String toString() {
        return "Strawberry{" +
            "type='" + type + '\'' +
            '}';
    }
}

class Orange extends Fruit implements Eatable {
    String type;
    Orange(String type) {
        this.type = type;
    }

    @Override
    public void eat() {
        System.out.println("I eat orange...");
    }

    @Override
    public String toString() {
        return "Orange{" +
            "type='" + type + '\'' +
            '}';
    }
}

class Apple extends Fruit {
    String type;
    Apple(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Apple{" +
            "type='" + type + '\'' +
            '}';
    }
}

class FruitBox<T extends Fruit> {
    T item;
    FruitBox(T item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "FruitBox{" +
            "item=" + item +
            '}';
    }
}

public class Main {
    public static void main(String[] args) {

        var apple = new Apple("papierówka");
        var appleBox = new FruitBox<>(apple);

        var apple2 = new Apple("antonówka");
        var appleBox2 = new FruitBox<>(apple2);

        var orange = new Orange("malinowy");
        var orangeBox = new FruitBox<>(orange);

        var strawberry = new Strawberry("ananasowy");
        var strawberryBox = new FruitBox<>(strawberry);

        System.out.println(appleBox);
        System.out.println(appleBox2);
        System.out.println(orangeBox);
        System.out.println(strawberryBox);

        // method(appleBox); // box zawiera owoc NIE implementujący interface Eatable
        // method(appleBox2);
        method(orangeBox);
        method(strawberryBox); // box zawiera owoc IMPLEMENTUJĄCY interface Eatable
    }

    // FruitBox<? extends Eatable> box
    // wymagany jest parametr FruitBox kóry w środku ma owoc (Fruit) implementujący interface Eatable
    private static void method(FruitBox<? extends Eatable> box) {
        box.item.eat();
    }
}

