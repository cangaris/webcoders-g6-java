package pl.cansoft.interfaces;

public class Cat implements Eat, Drink {
    @Override
    public void startEat() {
        System.out.println("I'm cat and I start eat...");
    }
    @Override
    public void endEat() {
        System.out.println("I'm cat and I end eat...");
    }
}
