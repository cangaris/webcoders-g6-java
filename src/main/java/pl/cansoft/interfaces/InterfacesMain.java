package pl.cansoft.interfaces;

public class InterfacesMain {
    public static void main(String[] args) {

        Pitbull dog = new Pitbull(); // nie moge utworzyć obiektu Dog bo jest abstract
        dog.startEat();
        dog.endEat();
        dog.startDrink();
        dog.endDrink();

        Pitbull pitbull = new Pitbull();
        print(pitbull);
        Eat cat = new Cat();
        print(cat);
        Turtle turtle = new Turtle();
        print(turtle);

        Eat.exampleStaticFunction();
    }

    private static void print(Eat eat) { // Pitbull, Cat, Turtle
        eat.startEat();
    }
}
