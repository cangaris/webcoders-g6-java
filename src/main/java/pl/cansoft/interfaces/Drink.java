package pl.cansoft.interfaces;

public interface Drink {
    default void startDrink() { // metoda domyślna (nie wymuszana na klasie) z własną implementacją, można nadpisywać
        printText("I start drink");
    }
    default void endDrink() {
        printText("I end drink");
    }
    private void printText(String textToPrint) { // metoda prywatna, pozwala uwspólnić kod z metod domyślnych
        System.out.println(textToPrint);
    }
}
