package pl.cansoft.interfaces;

public class Bird implements FlyEatDrink {
    @Override
    public void fly() {
    }
    @Override
    public void startDrink() {
    }
    @Override
    public void endDrink() {
    }
    @Override
    public void startEat() {
    }
    @Override
    public void endEat() {
    }
}
