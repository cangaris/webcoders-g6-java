package pl.cansoft.interfaces;

// klasa abstract - tylko do dziedziczenia, nie do tworzenia obiektów (nie Dog się nie uda)
public abstract class Dog implements Eat, Drink {
    @Override
    public void startEat() {
        System.out.println("I'm dog and I start eat...");
    }
    @Override
    public void endEat() {
        System.out.println("I'm dog and I end eat...");
    }
}
