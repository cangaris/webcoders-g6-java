package pl.cansoft.interfaces;

// final class - nie można dalej dziedziczyć po klasie Pitbull
public final class Pitbull extends Dog {
    // obiekt klasy Pitbull ma dostęp do wszystkich interfejsów klasy dog (metod i pól)
}
