package pl.cansoft.interfaces;

public interface Eat {
    String CONST_STRING = "I'm test string!"; // deklaracja stałej

    void startEat(); // deklaracja sygnatury (wymuszana na klasie)
    void endEat();

    static void exampleStaticFunction() {
        // funkcja statyczna, możemy używać bez tworzenia instancji (nowego obiektu (new))
    }
}
