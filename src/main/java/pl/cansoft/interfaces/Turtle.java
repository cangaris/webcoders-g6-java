package pl.cansoft.interfaces;

public class Turtle implements Eat, Drink {
    @Override
    public void startEat() {
        System.out.println("I'm turtle and I start eat...");
    }

    @Override
    public void endEat() {
        System.out.println("I'm turtle and I end eat...");
    }
}
