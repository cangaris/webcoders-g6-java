package pl.cansoft.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

class User {
    String name;
    User(String name) { this.name = name; }

    @Override
    public String toString() {
        return "User{" +
            "name='" + name + '\'' +
            '}';
    }
}

public class Main {
    public static void main(String[] args) {
        // List; // istotna jest dla nas kolejność ale nie unikalność
        // Set; // istotna jest dla nas unikalność ale nie kolejność
        // Map; // zestaw klucz-wartość -> { name: 'Damian', surname: 'Kowalski' }

        new ArrayList<>(); // impl interface List
        // jest wydajniejsza dla edytowania i odczytywania elementów po indexie
        // jest słabsza dla dodawania i usuwania elementów powyżej rozmiaru 10
        new LinkedList<>(); // impl interface List
        // jest wydajniejsza dla dodawania i usuwania elementów
        // jest lepsza do tych operacji gdy potrzebujemy listę większą niż 10
        new HashSet<>(); // impl interface Set
        new HashMap<>(); // impl interface Map

        var array = new HashSet<String>(); // List
        // System.out.println("Damian".hashCode());
        array.add("Damian"); // 2039715046
        // System.out.println("Asia".hashCode());
        array.add("Asia"); // 2050282
        // System.out.println("Basia".hashCode());
        array.add("Basia"); // 63955980
        array.add("Basia"); // 63955980
        array.add("Basia"); // 63955980

        array.remove("Basia");
        array.add("Tomek");

        System.out.println(array);

        // array.remove("Damian");
        // array.remove(0);
        // array.add(0, "Kasia"); // wstawienie Kasi na początek arraya
        // array.set(0, "Kasia"); // podmienia na konkretnym indeksie element
        // array.add(67867); // 67867 -> Integer.valueOf(67867)

        var users = new HashSet<User>();
        users.add(new User("Damian"));
        users.add(new User("Asia"));
        users.add(new User("Basia"));
        users.add(new User("Kasia"));

        // users.get(3) // array list wydajniejsza dla odczytu po indexie
        // array.set(3, "Kasia"); // array list wydajniejsza dla edycji po indexie

        var map = new HashMap<String, String>();
        map.put("age", "53");
        map.put("lastName", "Nowak");
        map.put("firstName", "Damian");

        var name = map.get("firstName");
        map.remove("lastName");
        map.replace("firstName", "Kasia");

        var polishCities = new HashSet<String>();
        polishCities.add("Warszawa");
        polishCities.add("Kraków");

        var germanCities = new HashSet<String>();
        germanCities.add("Berlin");
        germanCities.add("Hamburg");

        var countries = new HashMap<String, HashSet<String>>(); // mapa z nazwami państw jako klucz i zbiorem miast jako wartości
        countries.put("Polska", polishCities);
        countries.put("Niemcy", germanCities);
    }
}
