package pl.cansoft;

class Test extends Object {
    String name;
    void exampleMethod(String name) {
        this.name = name;
        System.out.println("hello!");
    }
} // extends Object - jest zbędne ponieważ dzieje się to tak czy siak automatycznie

public class Main {
    public static void main(String[] args) {

        Object test = (Object) new Test(); // (Object) - jest zbędne ponieważ dzieje się to tak czy siak automatycznie
        Object string = (Object) "test test test"; // new String("test test test");
        Integer zzz = (Integer) 1;
        Object object = new Object();

        // -------

        Object stringInstance = "string"; // 6 znaków

        System.out.println(stringInstance.getClass()); // class java.lang.Integer

        String realString = (String) stringInstance; // blad !!!

    }
}

class Main2 {
    public static void main(String[] args) {

        int intVariable = Integer.MAX_VALUE;
        long longVariable = Long.MAX_VALUE;

        methodLongArgument((int) longVariable); // (int)
        methodLongArgument(intVariable); // extending conversion
    }

    public static void methodLongArgument(int argument) {
        System.out.println(argument);
    }
}

class Main3 {

    public static void main(String[] args) {
        Test testRef = new Test(); // reference x
        Test testRef2 = testRef;
        Test testRef3 = testRef2;
        // testRef.exampleMethod("Test1");
        testRef2.exampleMethod("Test2");
//        testRef3.exampleMethod("Test3");


        testRef.name.toUpperCase();

        print(testRef);
        print(testRef2);
        print(testRef3);

        // Garbage Collector -> GC

        if (1 == 1) {
            int x = 5;
            // a tu x jest jeszcze potrzebny
        }
        // tu x jest nie potrzebny GC może ją zniszczyc

        int intRef1 = 10;
        int intRef2 = intRef1; // 10
        int intRef3 = intRef2; // 10
        intRef3 = 100; // 100
        System.out.println(intRef1);
        System.out.println(intRef2);
        System.out.println(intRef3);
        // !!! GC - usuwa ref i obiekty niepotrzebne
    }

    private static void print(Test test) {
        System.out.println(test.name);
    }
}
