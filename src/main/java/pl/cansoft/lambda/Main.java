package pl.cansoft.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Orange {}

public class Main {
    public static void main(String[] args) {

        // Predicate; // boolean test(T t);
        // Function; // R apply(T t);
        // Consumer; // void accept(T t);
        // Supplier; // T get();
        // UnaryOperator; // static <T> UnaryOperator<T> identity() { return t -> t; }

//        const x = [1, 2, 3]
//            .filter( v => v >= 2 )
//            .map( v => v * 2 );
//        console.log(x);

        Supplier<Orange> objectCreator1 = () -> new Orange(); // Supplier; // T get();
        Supplier<Orange> objectCreator2 = Orange::new;


        Predicate<Integer> predicateFn = v -> v >= 2;
        Function<Integer, Integer> mapFn = v -> v * 2;
        Consumer<Integer> printFn = v -> System.out.println(v);

        var streamedNumbers = List.of(1, 2, 3) // mamy listę integrów (listy nie można przetwarzać)
            .stream() // zamieniamy na strumień aby przetwarzać
            .filter( predicateFn ) // Predicate [1,2,3] -> [2,3] (usuwamy niepotrzebne elementy)
            .map( mapFn ) // Function [2,3] -> [4,6] (modyfiikujemy to co zostało)
            .collect(Collectors.toList()); // (Stream)[4,6] -> (List)[4,6] // (zamykamy strumień jako listę aby [rzypisać do zmiennej)

        streamedNumbers.add(9);
        System.out.println(streamedNumbers);
        // System.out::println TO SAMO v -> System.out.println(v)

        System.out.println("------");

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
        // const integerConsumer = v => console.log(v);
        Consumer<Integer> integerConsumer = v -> System.out.println(v);
        numbers.forEach( integerConsumer );
    }
}
