package pl.cansoft.equals;

class User { // extends Object
    String firstName;
    String lastName;
    User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        return firstName.hashCode() * 17 + lastName.hashCode() * 31;
    }

    @Override
    public boolean equals(Object obj) { // string
        if (obj == null || firstName == null || lastName == null) {
            return false;
        }
        // if (obj instanceof User) {
        //    User user = (User) obj; // Object
        if (obj instanceof User user) { // ta linijka to skrót dla 2 linijek zakomentowanych u góry
            return firstName.equals(user.firstName) && lastName.equals(user.lastName);
        }
        return false;
    }
}

public class Main {
    public static void main(String[] args) {

        // POROWNYWANIE STRINGOW

        String a1 = "test"; // string pool -> a5
        String a2 = "test"; // a5

        // System.out.println( a1 == a2 ); // a5 == a5 -> true (sprawdzenie czy ta sama ref)
        System.out.println( a1.equals(a2) ); // true (sprawdzenie czy ta sama wartość nawet gdy inna ref)

        String a = new String("test"); // e4
        String b = new String("test"); // f7

        // System.out.println( a == b ); // e4 != f7 -> false (sprawdzenie czy ta sama ref)
        System.out.println( a.equals(b) ); // true (sprawdzenie czy ta sama wartość nawet gdy inna ref)

        // POROWANANIE WLASNYCH OBIKETOW

        var user1 = new User("Asia", "Kowalska");
        var user2 = new User("Kasia", "Nowak"); // a7
        var user3 = new User("Kasia", "Nowak"); // b4

        System.out.println(user1);
        System.out.println(user2);
        System.out.println(user3);

        if (user2.equals(user3)) {
            System.out.println( "Obiekty są takie same" );
        } else {
            System.out.println( "Obiekty są różne" );
        }
    }
}

class Main2 {
    public static void main(String[] args) {
//        System.out.println( "Damian".hashCode() ); // 2039715046
//        System.out.println( "Damian2".hashCode() ); // -1193342964
//        System.out.println( "Dam ian".hashCode() ); // -1195510362
//        System.out.println( Integer.valueOf(11).hashCode() ); // 11
//        System.out.println( Integer.valueOf(191).hashCode() ); // 191
//        System.out.println( Integer.valueOf(1231).hashCode() ); // 191
//        System.out.println( Boolean.valueOf(true).hashCode() ); // 1231
//        System.out.println( Integer.valueOf(1237).hashCode() ); // 191
//        System.out.println( Boolean.valueOf(false).hashCode() ); // 1237

        System.out.println( new User("Damian", "Damian").hashCode() ); // 1
        System.out.println( new User("Damian", "Damian3").hashCode() ); // 1
    }
}
