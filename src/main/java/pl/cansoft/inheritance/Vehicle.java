package pl.cansoft.inheritance;

public class Vehicle {

    final String name; // final przy polu nie pozwala zmienić wartości po 1 zainicjalizowaniu

    public Vehicle(String name) {
        this.name = name;
    }

    public void go() {
        System.out.println("Vehicle - Go");
    }

    public void stop() {
        System.out.println("Vehicle - Stop");
        privateFunction();
    }

    // final przy metodzie -> nie można @Override w klasie potomnej
    // private bezModyfikatoraDostępu protected public
    protected final void privateFunction() {
    }
}
