package pl.cansoft.inheritance.main;

import pl.cansoft.inheritance.Car;
import pl.cansoft.inheritance.SUV;
import pl.cansoft.inheritance.Truck;
import pl.cansoft.inheritance.Vehicle;

public class InheritanceMain {
    public static void main(String[] args) {

        Car car1 = new Car("jakis name", "jakiś typ");
        car1.go();
        car1.stop();
        printCar(car1); // Car

        Car car2 = new Car("inny name", "inny typ");
        car2.go();
        car2.stop();
        printCar(car2); // Car

        Truck truck = new Truck("truck name", "track typ");
        truck.go();
        truck.stop();
        printCar(truck); // Truck

        SUV suv = new SUV("suv name", "suv typ", "suv brand");
        suv.go();
        suv.stop();
        printCar(suv); // SUV

        System.out.println("-----");
        System.out.println(car1);
        System.out.println(car2);
        System.out.println(truck);
        System.out.println(suv);
        System.out.println("-----");


        // ------ SUV -> Car -> Vehicle -? Object
        SUV suv2 = new SUV("suv name", "suv typ", "suv brand");
        printCar(suv2); // SUV
    }

    private static void printCar(Vehicle car) {
        System.out.println(car);
    }
}
