package pl.cansoft.inheritance;

public class SUV extends Car {

    String brand;

    public SUV(String name, String type, String brand) {
        super(name, type);
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "SUV{" +
            "brand='" + brand + '\'' +
            ", type='" + type + '\'' +
            ", name='" + name + '\'' +
            "} ";
    }
}
