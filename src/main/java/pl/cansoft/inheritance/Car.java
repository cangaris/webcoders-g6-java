package pl.cansoft.inheritance;

public class Car extends Vehicle {

    String type;

    public Car(String name, String type) {
        super(name);
        this.type = type;
    }

    @Override
    public void stop() {
        System.out.println("Car - Stop");
        super.privateFunction();
    }

    @Override
    public String toString() {
        return "Car{" +
            "type='" + type + '\'' +
            ", name='" + name + '\'' +
            "} ";
    }
}
