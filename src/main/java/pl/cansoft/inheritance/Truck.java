package pl.cansoft.inheritance;

public class Truck extends Vehicle {

    String type;

    public Truck(String name, String type) {
        super(name);
        this.type = type;
    }

    @Override
    public void go() {
        System.out.println("Truck - Go");
    }

    @Override
    public void stop() {
        // stop(); // fn stop z klasy Truck
        // this.stop(); // fn stop z klasy Truck
        super.stop(); // fn stop z klasy Vehicle
    }

    @Override
    public String toString() {
        return "Truck{" +
            "type='" + type + '\'' +
            ", name='" + name + '\'' +
            "} ";
    }
}
