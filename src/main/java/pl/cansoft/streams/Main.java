package pl.cansoft.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Game {
    public final String name;
    public final double rating;
    public final double price;
    public final int minPlayers;
    public final int maxPlayers;

    public Game(String name, double rating, double price, int minPlayers, int maxPlayers) {
        this.name = name;
        this.rating = rating;
        this.price = price;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }

    @Override
    public String toString() {
        return "Game{" +
            "name='" + name + '\'' +
            ", rating=" + rating +
            ", price=" + price +
            ", minPlayers=" + minPlayers +
            ", maxPlayers=" + maxPlayers +
            '}';
    }
}

public class Main {
    public static void main(String[] args) {

        // powinna pozwolić na grę w więcej niż 4 osoby,
        // powinna mieć ocenę wyższą niż 8,
        // powinna kosztować mniej niż 150 zł.
        List<Game> games = Arrays.asList(
            new Game("Terraforming Mars", 8.38, 123.49, 1, 5),
            new Game("Codenames", 7.82, 64.95, 2, 8),
            new Game("Puerto Rico", 8.07, 149.99, 2, 5),
            new Game("Terra Mystica", 8.26, 252.99, 2, 5),
            new Game("Scythe", 8.3, 314.95, 1, 5),
            new Game("Power Grid", 7.92, 145, 2, 6),
            new Game("7 Wonders Duel", 8.15, 109.95, 2, 2),
            new Game("Dominion: Intrigue", 7.77, 159.95, 2, 4),
            new Game("Patchwork", 7.77, 75, 2, 2),
            new Game("The Castles of Burgundy", 8.12, 129.95, 2, 4)
        );

        // Predicate<Game> minimumFivePeople = v -> v.maxPlayers >= 5;
        var modifiedGames = games.stream()
            .filter(game -> game.maxPlayers > 4)
            .filter(game -> game.rating > 8)
            .filter(game -> game.price < 150)
            .map(game -> game.name.toUpperCase())
            .collect(Collectors.toList());
            //.noneMatch( v -> v.length() > 4 ); czy żaden element nie spełni waruneku
            //.allMatch( v -> v.length() > 4 ); czy wszystkie elementy spełnią warunek
            //.anyMatch( v -> v.length() > 4 ); czy minimum 1 element spełni warunek

        System.out.println("-----");
        System.out.println(games);
        System.out.println("-----");
        System.out.println(modifiedGames);
        System.out.println("-----");
    }
}
