package pl.cansoft.streams;

import java.util.List;
import java.util.stream.Collectors;

public class Main2 {

    public static void main(String[] args) {

        List<String> names = List.of("Damian", "Kasia", "Basia", "Tomasz", "Janek");

        // todo: wyciągnąć listę tylko żeńskich imion
        // todo: pro tip: imiona zenskie koncza sie na "a" :)

        List<String> onlyWomanNames = names.stream() // krok 1 - konwersja do strumienia
                .filter(name -> name.endsWith("a")) // krok 2 - przechodzą dalej tylko kończące się na "a"
                .collect(Collectors.toList()); // krok 3 - zebranie ponowne danych ze strumienia do listy

        System.out.println(onlyWomanNames); // wynik: [Kasia, Basia]
    }
}

class Main3 {
    public static void main(String[] args) {

        List<Integer> numbers = List.of(1,2,3,4,5,6,7,8,9);

        // tip: parzyste -> reszta z dzielenia (modulo) == 0 :)
        // todo: wyciągnąć tylko parzyste, a potem pomnożyć przez 3
        List<Integer> results = numbers.stream()
                .filter( number -> number % 2 == 0 ) // [2, 4, 6, 8]
                .map( number -> number * 3 ) // [6, 12, 18, 24]
                .collect(Collectors.toList());

        System.out.println(results); // [6, 12, 18, 24]
    }
}

class User {
    String name;
    String role;

    User(String name, String role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
            "name='" + name + '\'' +
            ", role='" + role + '\'' +
            '}';
    }
}

class Main4 {

    public static void main(String[] args) {

        List<User> users = List.of(
            new User("Adam", "ADMIN"),
            new User("Piotr", "CLIENT"),
            new User("Asia", "ADMIN"),
            new User("Zosia", "MANAGER"),
            new User("Ania", "CLIENT")
        );

        // todo: wyciągnąć tylko ADMINów (filter) i zapisać do listy tylko ich imiona (map)
        List<String> results = users.stream()
                .filter( user -> user.role.equals("ADMIN") )
                .map( user -> user.name )
                .collect(Collectors.toList());

        System.out.println(results); // [Adam, Asia]
    }
}
