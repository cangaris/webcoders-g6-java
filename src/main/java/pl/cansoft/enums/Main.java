package pl.cansoft.enums;

class TShirt {
    String productName;
    Size productSize;
    TShirt(String productName, Size productSize) {
        this.productName = productName;
        this.productSize = productSize;
    }

    @Override
    public String toString() {
        return "TShirt{" +
            "productName='" + productName + '\'' +
            ", productSize=" + productSize +
            '}';
    }
}

public class Main {
    public static void main(String[] args) {

        var tShirt = new TShirt("jakaś nazwa", Size.S);

        switch (tShirt.productSize) { // S
            case M -> doSomething("Wybrano rozmiar M");
            case S -> doSomething("Wybrano rozmiar S");
            case L -> doSomething("Wybrano rozmiar L");
        }

        if (tShirt.productSize == Size.M) {
            System.out.println(tShirt.productSize.getLabel());
        }

        System.out.println(tShirt);
    }

    private static void doSomething(String text) {
        System.out.println(text);
    }
}
