package pl.cansoft.enums;

public enum Size {
       S(48, 71, 36, "Rozmiar S"),
       M(52, 74, 38, "Rozmiar M"),
       L(56, 76, 41, "Rozmiar L"),
       XL(61, 79, 41);

       private final int chestWidth;
       private final int shirtLength;
       private final int sleeveLength;
       private final String label;

       Size(int chestWidth, int shirtLength, int sleeveLength, String label) {
              this.chestWidth = chestWidth;
              this.shirtLength = shirtLength;
              this.sleeveLength = sleeveLength;
              this.label = label;
       }

       Size(int chestWidth, int shirtLength, int sleeveLength) {
              this.chestWidth = chestWidth;
              this.shirtLength = shirtLength;
              this.sleeveLength = sleeveLength;
              this.label = null; // gdyby pole label nie było finalne nie musiałbym nullować w konstruktorze
       }

       public int getChestWidth() {
              return chestWidth;
       }

       public int getShirtLength() {
              return shirtLength;
       }

       public int getSleeveLength() {
              return sleeveLength;
       }

       public String getLabel() {
              return label;
       }
}
