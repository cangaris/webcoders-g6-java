package pl.cansoft.cat.main;

import pl.cansoft.cat.Cat;

public class CatMain {
    public static void main(String[] args) {

        // Cat - klasa - plan na wytworzenie obiektu
        // new - ekipa budowlana
        // Cat - obiekt to realny byt w pamięci (dom zbudowany na działce X)
        var cat1 = new Cat("Zdzisiu", 9);
        cat1.meow();

        var cat2 = new Cat("Mruczek");
        cat2.run();

        System.out.println("-----");
        System.out.println(cat1);
        System.out.println(cat2);
        System.out.println("-----");

        cat1.setName("Eustachy");
        System.out.println("Imię kota nr 1 to: " + cat1.getName());

    }
}
