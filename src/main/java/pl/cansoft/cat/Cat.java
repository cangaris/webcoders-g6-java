package pl.cansoft.cat;

public class Cat {
    // wlasciwosci
    String name; // pole klasy, wlasciwosci klasy
    Integer age;
    String color;

    // konstruktor
    public Cat(String name) {
        this.name = name;
    }

    public Cat(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    // mozliwosci
    public void setName(String name) { // setter, metoda
        this.name = name;
    }

    public String getName() { // metoda, getter
        return name;
    }

    public void run() {
        System.out.println("Jestem " + name + " i biegam sobie!");
    }

    public void meow() {
        System.out.println("Jestem " + name + " i miałcze sobie!");
    }

    @Override
    public String toString() {
        return "Cat{" +
            "name='" + name + '\'' +
            ", age=" + age +
            '}';
    }
}
