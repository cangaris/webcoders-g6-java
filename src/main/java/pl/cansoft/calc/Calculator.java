package pl.cansoft.calc;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        // inicjowanie skanera
        var param = System.in; // in jest statyczna
        var scanner = new Scanner(param);

        // pobranie danej A od usera
        System.out.print("Wpisz liczbę A: ");
        var dataA = 0;
        try {
            dataA = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Niepoprawne dane");
            return;
        }

        // pobranie danej B od usera
        System.out.print("Wpisz liczbę B: ");
        var dataB = 0;
        try {
            dataB = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Niepoprawne dane");
            return;
        }

        // sumowanie danych A i B
        var sum = dataA + dataB;
        System.out.println("Wynik działania to: " + sum);
    }
}
