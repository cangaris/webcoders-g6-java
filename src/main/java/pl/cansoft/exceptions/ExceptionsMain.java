package pl.cansoft.exceptions;

class DatabaseGetDataException extends Exception { // gdy błąd dziedziczy po Exception MUSI być łapany
    DatabaseGetDataException() {
        super("Could not get data from database");
    }
}
class DatabaseConnectionException extends RuntimeException { // gdy błąd dziedziczy po RuntimeException NIE musi być łapany
    DatabaseConnectionException() {
        super("Could not connect to database");
    }
}

class DatabaseConnector {
    void connectToDatabase() { // unchecked !
        throw new DatabaseConnectionException();
        // System.out.println("Connected to database");
    }
    String[] getUsers() throws DatabaseGetDataException { // checked !
        throw new DatabaseGetDataException();
        // return new String[] {"Damian", "Kasia", "Asia"};
    }
}

public class ExceptionsMain {
    public static void main(String[] args) {
        var dbConn = new DatabaseConnector();
        try {
            dbConn.connectToDatabase(); // DatabaseConnectionException
            var users = dbConn.getUsers(); // DatabaseGetDataException
            for (var user : users) {
                System.out.println(user);
            }
        } catch (DatabaseConnectionException | DatabaseGetDataException exception) {
            System.out.println("Error: " + exception.getMessage());
        }
    }
}
